//
// Created by owmess on 2021/12/20.
//
#include "FindOnePath.h"

FindOnePath::FindOnePath(const vector <vector<char>> map, int width, int height) {
    this->width_ = width;
    this->height_ = height;
    findFlag_ = false;
    map_ = map;
}

void FindOnePath::DFSMove(int startX, int startY, int endX, int endY, int step) {
    path_.push_back(Path{step, startX, startY}); ///记路径
    map_[startX][startY] = WALL;   ///走过的路径标记为!
    if ((startX == endX) && (startY == endY)) {
        findFlag_ = true;
    } else {
        if ((!findFlag_) && (startY != width_ - 1) && (map_[startX][startY + 1] == WAY))
            DFSMove(startX, startY + 1, endX, endY, step + 1);         ///向右
        if ((!findFlag_) && (startX != height_ - 1) && (map_[startX + 1][startY] == WAY))
            DFSMove(startX + 1, startY, endX, endY, step + 1);   ///往下
        if ((!findFlag_) && (startY != 0) && (map_[startX][startY - 1] == WAY))
            DFSMove(startX, startY - 1, endX, endY, step + 1);   ///往左
        if ((!findFlag_) && (startX != 0) && (map_[startX - 1][startY] == WAY))
            DFSMove(startX - 1, startY, endX, endY, step + 1);   ///往上
    }
}

void FindOnePath::findByDFS(int startX, int startY, int endX, int endY, int step) {
    DFSMove(startX, startY, endX, endY, step);
    if (findFlag_) {
        for (const auto &i: path_) { printf("(%d,%d) ", i.x, i.y); }
        cout << endl;
    } else {
        cout << "No way!" << endl;
    }
}

void FindOnePath::findByBFS(int startX, int startY, int endX, int endY) {
    int head = 0;
    map_[startX][startY] = WALL;
    path_.push_back(Path{head, startX, startY});
    while (head != path_.size()) {
        for (size_t i = 0; i < 4; i++) {
            Path n = Path{0, path_[head].x + u_[i], path_[head].y + v_[i]};
            if (n.x >= 0 && n.y >= 0 && n.x <= height_ - 1 && n.y <= width_ - 1) {
                if (map_[n.x][n.y] == WAY) {
                    path_.push_back(Path{head, n.x, n.y});
                    map_[n.x][n.y] = WALL;
                    if ((n.x == endX) && (n.y == endY)) {     ///扩展出的结点为目标结点
                        findFlag_ = true;
                        BFSPrint(path_.size() - 1);
                        cout << endl;
                        break;
                    }
                }
            }
        }
        head++;
        if (findFlag_)break;
    }
    if (!findFlag_)
        cout << "No way!" << endl;
}

void FindOnePath::BFSPrint(int d) {
    if (path_[d].step != 0) BFSPrint(path_[d].step);                ///递归输出路径
    printf("(%d,%d) ", path_[d].x, path_[d].y);
}

//void FindOnePath::findByAstar(int startX,int startY,int endX,int endY){
//    vector<Node> openList;
//    vector<Node> closeList;
//    openList.push_back(Node{startX,startY,0,0,0});
//    for(size_t i=0;i<4;i++){
//        openList[]
//    }

//}



