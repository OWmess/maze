//
// Created by owmess on 2021/12/18.
//
#include "Menu.h"
int main() {
    char input = 'p';
    while (input != 'e') {
        Menu::showMenu("../menu.txt");
        fflush(stdin);
        cin >> input;
        switch (input) {
            case '0':
                Menu::task0();
                break;
            case '1':
                Menu::task1();
                break;
            case '2':
                Menu::task2();
                break;
            case '3':
                Menu::task3();
                break;
            case '4':
                Menu::task4();
                break;
            case '5':
                Menu::task5();
                break;
            case '6':
                Menu::task6();
                break;
            case '7':
                Menu::task7();
                break;
            case 'e':
                break;
            default:
                cout << "输入有误，请重新输入！" << endl;
                break;
        }
    }
    std::cout << "已退出程序！" << std::endl;
    return 0;
}

