//
// Created by owmess on 2021/12/21.
//
#include "FindAllPath.h"

void FindAllPath::dispapath() {
    int k;
    bool flag = false;
    printf("%2d: ", count_++);
    if (top_ < mintop_) {
        mintop_ = top_;
        flag = true;
        minpath_.clear();
    }
    for (k = 0; k <= top_; k++) {
        printf("(%d,%d)", St[k].i, St[k].j);
        if (flag) {
            minpath_.push_back(Point{St[k].i, St[k].j});
        }
    }
    cout << endl;
}

void FindAllPath::findPath(int xi, int yi, int xe, int ye) {
    int i, j, i1, j1, di;
    bool find;
    top_++;
    St[top_].i = xi;
    St[top_].j = yi;
    St[top_].di = -1;
    map_[xi][yi] = -1;
    while (top_ > -1) {
        i = St[top_].i;
        j = St[top_].j;
        di = St[top_].di;
        if (i == xe && j == ye) {
            dispapath();
            map_[i][j] = WAY;
            top_--;
            i = St[top_].i;
            j = St[top_].j;
            di = St[top_].di;
        }
        find = false;
        while (di < 4 && !find) {
            di++;
            switch (di) {
                case 0:
                    i1 = i - 1;
                    j1 = j;
                    break;
                case 1:
                    i1 = i;
                    j1 = j + 1;
                    break;
                case 2:
                    i1 = i + 1;
                    j1 = j;
                    break;
                case 3:
                    i1 = i;
                    j1 = j - 1;
                    break;
            }
            if (i1 < 0 || j1 < 0 || i1 > height_ - 1 || j1 > width_ - 1) continue;
            if (map_[i1][j1] == WAY) find = true;
        }
        if (find) {
            St[top_].di = di;
            top_++;
            St[top_].i = i1;
            St[top_].j = j1;
            St[top_].di = -1;
            map_[i1][j1] = -1;
        } else {
            map_[i][j] = WAY;
            top_--;
        }

    }
}

void FindAllPath::findBestPath(int xi, int yi, int xe, int ye) {
    findPath(xi, yi, xe, ye);
    cout << "best path is:" << endl;
    for (const auto &b: minpath_) {
        printf("(%d,%d)", b.x, b.y);
    }
    cout << endl;
}
