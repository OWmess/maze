//
// Created by owmess on 2021/12/19.
//

#include "Map.h"

Map::Map(const string &mapFile) {
    ///开辟map空间
    mapFile_ = mapFile;
    map_.resize(9);
    for (int k = 0; k < 9; ++k) {
        map_[k].resize(9);
    }
    ifstream myFile(mapFile_);
    if (!myFile.is_open())
        return;
    char ch;
    size_t i = 0, x = 0;
    while (!myFile.eof() && i != 9) {
        myFile >> ch;
        int tmp = ch;
        map_[i][x] = tmp;
        x++;
        if (x == 9)x = 0, i = i + 1;
    }
    myFile.close();
}