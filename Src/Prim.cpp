//
// Created by owmess on 2021/12/19.
//
#include"Prim.h"

using namespace std;

//Prim随机生成迷宫构造函数
Prim::Prim(int width, int height) {
    _width = width;
    _height = height;
    map_.resize(height);
    for (int k = 0; k < height; ++k) {
        map_[k].resize(width);
    }
    for (int i = 0; i < height; i++) {//初始化全部赋为'0'
        for (int j = 0; j < width; j++) {
            map_[i][j] = '0';
        }
    }
}

Prim::~Prim() {
}

/*
Prim随机生成迷宫算法
1.让迷宫全是墙（ 实现: 外部调用 Prim::createMaze）
2.选一个单元格作为迷宫的通路(在此选择起点)，然后把它的邻墙（上下左右）放入列表
3.当列表里面还有墙时:
①.从列表里随机选一个墙，如果这面墙分隔的两个单元格只有一个单元格被访问过
	（a）从列表里面移除这面墙，这样就把墙打通了，让为访问过的单元格称为迷宫的通路
	（b）把这个格子的墙加入列表
②.如果墙两面的单元格都已经被访问过（都打通了），那就从列表里移除这面墙
*/
void Prim::algorithm() {
    srand((unsigned) time(NULL));//随机数种子
    Point start, end;
    struct Block pathfinder;
    start.x = 1;//起点
    start.y = 1;
    pathfinder.x = start.x;//探路者
    pathfinder.y = start.y;
    end.x = _width - 2;//终点
    end.y = _height - 2;
    vector<struct Block> list;//存放点
    //选起点作为迷宫的通路，将其它的上下左右放入列表
    addBlockToList(list, pathfinder);
    while (list.size()) {
        int randnum = rand() % list.size();
        Block selectBlock = list[randnum];
        //探路者来到“选择的墙”这里
        pathfinder = selectBlock;
        //根据当前选择的墙的方向进行后续操作
        //此时，起始点 选择的墙 目标块 三块区域在同一直线上
        //我们让矿工从“选择的墙”继续前进到“目标块”
        //矿工有穿墙能力 ：)
        switch (selectBlock.direction) {
            case 1: {//下
                pathfinder.y++;
                break;
            }
            case 3: {//右
                pathfinder.x++;
                break;
            }
            case 2: {//左
                pathfinder.x--;
                break;
            }
            case 0: {//上
                pathfinder.y--;
                break;
            }
        }
        //目标块如果是墙
        if (map_[pathfinder.y][pathfinder.x] == '1') {
            //打通墙和目标块
            map_[selectBlock.y][selectBlock.x] = map_[pathfinder.y][pathfinder.x] = '0';
            //再次找出与矿工当前位置相邻的墙
            addBlockToList(list, pathfinder);
        } else {//如果不是呢？说明我们的矿工挖到了一个空旷的通路上面 休息一下就好了
            //relax
        }
        //删除这堵墙(把用不了的墙删了，对于那些已经施工过了不必再施工了，同时也是确保我们能跳出循环)
        list.erase(list.begin() + randnum);
    }
}

//将指定的点的上下左右合法的点加入到列表中
void Prim::addBlockToList(vector<struct Block> &list, struct Block center) {
    struct Block p;
    p.x = center.x + 1;//右
    p.y = center.y;
    if (p.x >= 1 && p.y >= 1 && p.x <= _width - 2 && p.y <= _height - 2 && map_[p.y][p.x] == '1') {
        p.direction = 3;
        list.push_back(p);
    }
    p.x = center.x - 1;//左
    p.y = center.y;
    if (p.x >= 1 && p.y >= 1 && p.x <= _width - 2 && p.y <= _height - 2 && map_[p.y][p.x] == '1') {
        p.direction = 2;
        list.push_back(p);
    }
    p.x = center.x;//上
    p.y = center.y - 1;
    if (p.x >= 1 && p.y >= 1 && p.x <= _width - 2 && p.y <= _height - 2 && map_[p.y][p.x] == '1') {
        p.direction = 0;
        list.push_back(p);
    }
    p.x = center.x;//下
    p.y = center.y + 1;
    if (p.x >= 1 && p.y >= 1 && p.x <= _width - 2 && p.y <= _height - 2 && map_[p.y][p.x] == '1') {
        p.direction = 1;
        list.push_back(p);
    }
}

//刷新迷宫函数
void Prim::createMaze(int startX, int startY, int endX, int endY) {
    //让迷宫全部都为墙
    for (int i = 0; i < _height; i++) {
        for (int j = 0; j < _width; j++) {
            map_[i][j] = '1';
        }
    }
    map_[1][0] = '1';
    map_[endX][endY] = '0';//粮仓位置
    //起点置为'0'
    map_[startX][startY] = '0';
    algorithm();//Prim随机生成迷宫算法
}
