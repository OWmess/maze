//
// Created by owmess on 2021/12/19.
//

#ifndef MAZE_MAP_H
#define MAZE_MAP_H

#include "headfiles/mazeDefine.h"

class Map {
public:
    ~Map() = default;

    Map(const string &mapFile);

    /*
     *获取迷宫
     */
    vector <vector<char>> getMap() {
        return map_;
    }

private:
    vector <vector<char>> map_;
    string mapFile_;
};


#endif //MAZE_MAP_H
