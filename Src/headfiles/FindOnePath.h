//
// Created by owmess on 2021/12/20.
//

#ifndef MAZE_FINDONEPATH_H
#define MAZE_FINDONEPATH_H

#include "headfiles/mazeDefine.h"

class FindOnePath {
public:
    FindOnePath(const vector <vector<char>> map, int width = 9, int height = 9);

    ~FindOnePath() = default;



    ///DFS查找路径
    void findByDFS(int startX, int startY, int endX, int endY, int step = 1);
    ///BFS查找路径
    void findByBFS(int startX, int startY, int endX, int endY);
//    void findByAstar(int startX,int startY,int endX,int endY);
private:
    /*
     * 路径
     */
    struct Path {
        int step = 0;
        int x = 0;
        int y = 0;
    };
    const int u_[4] = {0, 1, 0, -1};
    const int v_[4] = {1, 0, -1, 0};
    int height_ = 8;
    int width_ = 8;
    void DFSMove(int startX, int startY, int endX, int endY, int step);
    ///DFS递归寻找路径
    void BFSPrint(int d);

    vector <vector<char>> map_;
    ///用于存储路径
    vector <Path> path_;
    bool findFlag_;
};


#endif //MAZE_FINDONEPATH_H
