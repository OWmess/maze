//
// Created by q1162 on 2021/12/22.
//

#ifndef MAZE_MENU_H
#define MAZE_MENU_H

#include "mazeDefine.h"

class Menu {
public:
    Menu() = default;
    static bool showMenu(const string &myFile);
    ///深度搜索9*9迷宫
    static void task0();
    ///广度搜索9*9迷宫
    static void task1();
    ///求9*9迷宫的所有可行路径
    static void task2();
    ///求9*9迷宫的最短路径
    static void task3();
    ///广度搜索prim随机生成的90*90迷宫
    static void task4();
    ///深度搜索prim随机生成的90*90迷宫
    static void task5();
    ///求height*width迷宫的所有可行路径
    static void task6();
    ///求height*width迷宫的最短可行路径
    static void task7();
};


#endif //MAZE_MENU_H
