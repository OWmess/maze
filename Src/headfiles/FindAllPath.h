//
// Created by owmess on 2021/12/21.
//

#ifndef MAZE_FINDALLPATH_H
#define MAZE_FINDALLPATH_H

#include "mazeDefine.h"

#define MaxSize 1000

class FindAllPath {
public:
    FindAllPath(const vector<vector<char>> &map, int height = 9, int width = 9) {
        this->height_ = height;
        this->width_ = width;
        map_ = map;
    }
    void findPath(int xi, int yi, int xe, int ye);
    void findBestPath(int xi, int yi, int xe, int ye);
private:
    int height_;
    int width_;
    vector<vector<char>> map_;
    struct {
        int i, j;
        int di;
    } St[MaxSize]{}, Path[MaxSize]{};
    struct Point {
        int x;
        int y;
    };
    vector<Point> minpath_;
    int top_ = -1;
    int count_ = 1;
    int mintop_ = 65535;

    ///用于存储路径
    void dispapath();

};


#endif //MAZE_FINDALLPATH_H
