//
// Created by owmess on 2021/12/19.
//
#pragma once
#ifndef __PRIM_H__
#define __PRIM_H__

#include <mazeDefine.h>

class Prim {
    struct Point {
        int x;
        int y;
    };
    struct Block {
        int x;
        int y;
        int direction;//0 up ,1 down,2 left,3right
    };
private:
    int _width, _height;
    vector<vector<char>> map_;

    void algorithm();

    void addBlockToList(std::vector<struct Block> &list, struct Block p);

public:
    Prim(int width, int height);

    ~Prim();

    void createMaze(int startX, int startY, int endX, int endY);//ˢ���Թ�
    vector<vector<char>> getMap() {
        return map_;
    }
};


#endif