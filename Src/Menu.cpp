//
// Created by q1162 on 2021/12/22.
//
#include "Map.h"
#include "FindOnePath.h"
#include "FindAllPath.h"
#include "Prim.h"
#include "Menu.h"

bool Menu::showMenu(const string &myfile) {
    ifstream myFile(myfile);
    if (!myFile.is_open())
        return false;
    string s;
    while (getline(myFile, s)) {
        cout << s << endl;
    }
    myFile.close();
    cout << endl;
}

void Menu::task0() {
    cout << "输入入口和出口，格式为：startX，startY，endX，endY" << endl;
    int sx, sy, ex, ey;
    scanf("%d,%d,%d,%d", &sx, &sy, &ex, &ey);
    cout << endl;
    Map _map("../maze.txt");
    FindOnePath _find(_map.getMap(), 9, 9);
    _find.findByBFS(sx, sy, ex, ey);
    system("pause");
}

void Menu::task1() {
    cout << "输入入口和出口，格式为：startX，startY，endX，endY" << endl;
    int sx, sy, ex, ey;
    scanf("%d,%d,%d,%d", &sx, &sy, &ex, &ey);
    cout << endl;
    Map _map("../maze.txt");
    FindOnePath _find(_map.getMap(), 9, 9);
    _find.findByDFS(sx, sy, ex, ey);
    system("pause");
}

void Menu::task2() {
    cout << "输入入口和出口，格式为：startX，startY，endX，endY" << endl;
    int sx, sy, ex, ey;
    scanf("%d,%d,%d,%d", &sx, &sy, &ex, &ey);
    cout << endl;
    Map _map("../maze.txt");
    FindAllPath _find(_map.getMap(), 9, 9);
    _find.findPath(sx, sy, ex, ey);
    system("pause");
}

void Menu::task3() {
    cout << "输入入口和出口，格式为：startX，startY，endX，endY" << endl;
    int sx, sy, ex, ey;
    scanf("%d,%d,%d,%d", &sx, &sy, &ex, &ey);
    cout << endl;
    Map _map("../maze.txt");
    FindAllPath _find(_map.getMap(), 9, 9);
    _find.findBestPath(sx, sy, ex, ey);
    system("pause");
}

void Menu::task4() {
    cout << "输入迷宫的尺寸，格式为：height，width" << endl;
    int height, width;
    scanf("%d,%d", &height, &width);
    cout << endl;
    cout << "输入入口和出口，格式为：startX，startY，endX，endY" << endl;
    int sx, sy, ex, ey;
    scanf("%d,%d,%d,%d", &sx, &sy, &ex, &ey);
    cout << endl;
    Prim _primMap(width, height);
    _primMap.createMaze(sx, sy, ex, ey);
    FindOnePath _find(_primMap.getMap(), width, height);
    _find.findByBFS(sx, sy, ex, ey);
    system("pause");
}

void Menu::task5() {
    cout << "输入迷宫的尺寸，格式为：height，width" << endl;
    int height, width;
    scanf("%d,%d", &height, &width);
    cout << endl;
    cout << "输入入口和出口，格式为：startX，startY，endX，endY" << endl;
    int sx, sy, ex, ey;
    scanf("%d,%d,%d,%d", &sx, &sy, &ex, &ey);
    cout << endl;
    Prim _primMap(width, height);
    _primMap.createMaze(sx, sy, ex, ey);
    FindOnePath _find(_primMap.getMap(), width, height);
    _find.findByDFS(sx, sy, ex, ey);
    system("pause");
}

void Menu::task6() {
    cout << "输入迷宫的尺寸，格式为：height，width" << endl;
    int height, width;
    scanf("%d,%d", &height, &width);
    cout << endl;
    cout << "输入入口和出口，格式为：startX，startY，endX，endY" << endl;
    int sx, sy, ex, ey;
    scanf("%d,%d,%d,%d", &sx, &sy, &ex, &ey);
    cout << endl;
    Prim _primMap(width, height);
    _primMap.createMaze(sx, sy, ex, ey);
    FindAllPath _find(_primMap.getMap(), width, height);
    _find.findPath(sx, sy, ex, ey);
    system("pause");
}

void Menu::task7() {
    cout << "输入迷宫的尺寸，格式为：height，width" << endl;
    int height, width;
    scanf("%d,%d", &height, &width);
    cout << endl;
    cout << "输入入口和出口，格式为：startX，startY，endX，endY" << endl;
    int sx, sy, ex, ey;
    scanf("%d,%d,%d,%d", &sx, &sy, &ex, &ey);
    cout << endl;
    Prim _primMap(width, height);
    _primMap.createMaze(sx, sy, ex, ey);
    FindAllPath _find(_primMap.getMap(), width, height);
    _find.findBestPath(sx, sy, ex, ey);
    system("pause");
}